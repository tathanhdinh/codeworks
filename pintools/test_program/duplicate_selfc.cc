#include <Windows.h>
#include <tchar.h>

#include <string>
#include <iostream>

int _tmain(int argc, TCHAR *argv[])
{
  // get full path of current executing module
  TCHAR szFilePath[MAX_PATH];
  GetModuleFileName(NULL, szFilePath, MAX_PATH);

  // print out the path
  std::wcout << szFilePath << std::endl;

  // generate new path for new file
  std::wstring new_file_path(szFilePath);
  if (new_file_path.size() > 4) new_file_path.resize(new_file_path.size() - 4);
  new_file_path += L"_new"; new_file_path += L".exe";

  // using convenient copy function
  CopyFile(szFilePath, new_file_path.c_str(), FALSE);

  //// open current file and get the handle
  //HANDLE hFileHandle;
  //hFileHandle = CreateFile(szFilePath, GENERIC_READ, FILE_SHARE_READ, NULL, 
  //                         OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

  //// create the new file and get the handle
  //HANDLE hNewFileHandle;
  //hNewFileHandle = CreateFile(new_file_path.c_str(), GENERIC_WRITE, 0, NULL, 
  //                            CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

  //// copy file
  //CHAR buffer[512];
  //DWORD nbRead, nbWrite;
  //while (ReadFile(hFileHandle, (LPVOID)buffer, 512, &nbRead, NULL) && (nbRead > 0)) {
  //  WriteFile(hNewFileHandle, buffer, nbRead, &nbWrite, NULL);
  //}

  //// remember to close handles
  //CloseHandle(hFileHandle); CloseHandle(hNewFileHandle);

  return 0;
}