#include <Windows.h>
#include <tchar.h>

#include <string>
#include <iostream>

int _tmain(int argc, TCHAR *argv[])
{
  std::wstring file_path(L"1.txt");
  std::wstring new_file_path(L"2.txt");
  std::wstring backup_file_path = file_path + L".backup";

  BOOL result;
  result = ::ReplaceFile(file_path.c_str(), new_file_path.c_str(), backup_file_path.c_str(), 
                         REPLACEFILE_WRITE_THROUGH, NULL, NULL);
  if (result != 0) {
    std::wcout << L"Original file: " << file_path << std::endl
               << L"Replaced file: " << new_file_path << std::endl
               << L"Backup file: " << backup_file_path << std::endl;
  }
  else {
    std::wcout << L"ReplaceFile failed!" << std::endl;
  }

  return 0;
}