#include <Windows.h>
#include <UrlMon.h>

#include <tchar.h>
#include <string>
#include <iostream>

#pragma comment(lib, "urlmon.lib")

int _tmain(int argc, TCHAR *argv[])
{
  std::wstring remote_url = L"http://news.ycombinator.com/";
  std::wstring downloaded_file = L"downloaded";

  HRESULT hRes = URLDownloadToFile(NULL, remote_url.c_str(), downloaded_file.c_str(), 0, NULL);
  if (hRes == S_OK) std::cout << "Success" << std::endl;
  else std::cout << "Unsucess" << std::endl;

  return 0;
}