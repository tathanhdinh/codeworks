#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <tchar.h>

#include <iostream>

#pragma comment(lib, "Ws2_32.lib")

int _tmain(int argc, TCHAR *argv[])
{
  WSADATA wsaData;
  ::WSAStartup(MAKEWORD(2, 2), &wsaData);

  static const char port[] = "8080";
  struct addrinfo *result = NULL, *ptr = NULL, hints = {};
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;
  hints.ai_flags = AI_PASSIVE;
  ::getaddrinfo(NULL, port, &hints, &result);

  SOCKET listen_socket = INVALID_SOCKET;
  listen_socket = ::socket(result->ai_family, result->ai_socktype, result->ai_protocol);

  ::bind(listen_socket, result->ai_addr, result->ai_addrlen);
  ::listen(listen_socket, SOMAXCONN);

  /*struct sockaddr_in server_address;
  int address_length = sizeof(struct sockaddr_in);
  ::getsockname(listen_socket, reinterpret_cast<sockaddr*>(&server_address), &address_length);
  std::wcout << L"Listening port: " << static_cast<int>(::ntohs(server_address.sin_port)) << std::endl;*/

  SOCKET client_socket = INVALID_SOCKET;
  client_socket = ::accept(listen_socket, NULL, NULL);

  ::closesocket(client_socket);
  ::WSACleanup();

  return 0;
}
