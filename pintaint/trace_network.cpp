#include "pin.H"
#include "instlib.H"
#include "portability.H"
// #include <image.PH>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <map>
#include <sstream>
#include <cstdlib>
#include <ctime>
// #include <random>
// #include <tr1/memory>
// #include <cstdint>

/*===================================================================================================================*/
/*                                                 global declaration                                                */
/*===================================================================================================================*/
ADDRINT const sendto_id = 44;
ADDRINT const recvfrom_id = 45;

ADDRINT syscall_id;
ADDRINT args[6];

char *buffer_data = 0;
unsigned int buffer_length = 0;

// bool new_instruction;
// std::string current_instruction;

std::ofstream log_file;
std::ofstream dynamic_instruction_log_file;
std::ofstream static_instruction_log_file;

bool is_read = false;
bool is_write = false;

bool first_message_received = false;

std::map<ADDRINT, std::string> addr_ins_static_map;
std::map<ADDRINT, bool> printed_instructions;

#define MODIFIED_BYTE_NUMBER 50

#define CALL_INSTRUCTION 0
#define INDIRECT_JUMP_INSTRUCTION 1
#define RETURN_INSTRUCTION 2

std::map<ADDRINT, unsigned char> instruction_type;

/*===================================================================================================================*/
/*                                                   tool functions                                                  */
/*===================================================================================================================*/
/* convert a binary buffer to a readable hex string */
std::string buffer_to_hex_string(char *buffer_data, unsigned int buffer_length)
{
  std::ostringstream ss;
  ss << std::hex << std::setfill('0');
  for (unsigned int i = 0; i < buffer_length; i++) {
    ss << std::setw(2) << static_cast<int>(buffer_data[i]);
  }

  return ss.str();
}

/* ------------------------------------------------------------------------- */
/* modifier le tampon de façon aléatoire                                     */
/* ------------------------------------------------------------------------- */
void modify_data(char *buffer_data, unsigned int buffer_length)
{
  std::srand(std::time(0));

//  unsigned int top = std::max<unsigned int>(MODIFIED_BYTE_NUMBER, buffer_length);
  unsigned int bottom = std::min<unsigned int>(MODIFIED_BYTE_NUMBER, buffer_length);
  unsigned int random_index;

  for (unsigned int i = 0; i < bottom; ++i) {
    random_index = std::rand() % buffer_length;
    std::cout << "modifie l'octet " << random_index << " de "
              << static_cast<int>(buffer_data[random_index]);

    buffer_data[random_index] = std::rand() % 256;
    std::cout << " à " << static_cast<int>(buffer_data[random_index]) << std::endl;
  }
  
  return;
}

/* ------------------------------------------------------------------------- */
/* log the sendto syscall                                                    */
/* ------------------------------------------------------------------------- */
void log_sendto()
{
  buffer_data = reinterpret_cast<char*>(args[1]);
  buffer_length = args[2];
  
  log_file << "==============================================" << std::endl;
  log_file << "sendto" << std::endl << "buffer address: " << static_cast<void*>(buffer_data) 
           << ", length: " << buffer_length << std::endl;
  log_file << buffer_to_hex_string(buffer_data, buffer_length) << std::endl;
  log_file << "==============================================" << std::endl;
  
  return;
}

/* ------------------------------------------------------------------------- */
/* log the recvfrom syscall                                                  */
/* ------------------------------------------------------------------------- */
void log_recvfrom()
{
  buffer_data = reinterpret_cast<char*>(args[1]);
  buffer_length = args[2];
  
  log_file << "==============================================" << std::endl;
  log_file << "recvfrom" << std::endl << "buffer address: " << static_cast<void*>(buffer_data) 
           << ", length: " << buffer_length << std::endl;
  log_file << buffer_to_hex_string(buffer_data, buffer_length) << std::endl;
  log_file << "==============================================" << std::endl;
  
  //   is_received = true;
  return;
}

/* ------------------------------------------------------------------------- */
/* modify the received data of recvfrom syscall                              */
/* ------------------------------------------------------------------------- */
void mod_recvfrom() 
{
  buffer_data = reinterpret_cast<char*>(args[1]);
  buffer_length = args[2];
  
  modify_data(buffer_data, buffer_length);
  
  return;
}

/* ------------------------------------------------------------------------- */
/* verify if the read/write memory is in captured buffer                     */
/* ------------------------------------------------------------------------- */
bool is_inside(VOID *address)
{
  ADDRINT addr = reinterpret_cast<ADDRINT>(address);
  ADDRINT bf = reinterpret_cast<ADDRINT>(buffer_data);
  
  if ((addr >= bf) && (addr < bf + buffer_length)) return true;
  else return false;
}

/* ------------------------------------------------------------------------- */
/* detected contained library                                                */
/* (modified from the Zynamics shellcode detection tool)                     */
/* ------------------------------------------------------------------------- */
std::string contained_library(ADDRINT address)
{
  std::string lib_name = "";
  for (IMG img = APP_ImgHead(); IMG_Valid(img); img = IMG_Next(img)) {
    for (SEC sec = IMG_SecHead(img); SEC_Valid(sec); sec = SEC_Next(sec)) {
      if (address >= SEC_Address(sec) && address < SEC_Address(sec) + SEC_Size(sec)) {
        lib_name = IMG_Name(img);
        break;
      }
    }
    if (lib_name != "") break;
  }
  return lib_name;
}


/*===================================================================================================================*/
/*                                                 analysis routines                                                 */
/*===================================================================================================================*/
/* ------------------------------------------------------------------------- */
/* log the execution of an instruction                                       */
/* ------------------------------------------------------------------------- */
void log_instruction(VOID *ip_address, VOID *r_address, VOID *w_address)
{
  return;
}

/* ------------------------------------------------------------------------- */
/* log routine                                                               */
/* ------------------------------------------------------------------------- */
void log_simple(ADDRINT ins_addr)
{
  dynamic_instruction_log_file << std::setfill(' ') << std::setw(16);
  dynamic_instruction_log_file << reinterpret_cast<VOID*>(ins_addr) << "  " << addr_ins_static_map[ins_addr] 
                               << std::endl;
  
  return;
}

/* ------------------------------------------------------------------------- */
/* log dynamic and static (i.e. without replication) trace                   */
/* ------------------------------------------------------------------------- */
void log_static_and_dynamic(ADDRINT ins_addr) 
{
  if (first_message_received) {
    // static trace
    if (printed_instructions.find(ins_addr) == printed_instructions.end()) {
//      static_instruction_log_file << std::setfill(' ') << std::setw(30) << contained_library(ins_addr);
//      static_instruction_log_file << std::setfill(' ') << std::setw(16);
      static_instruction_log_file << reinterpret_cast<VOID*>(ins_addr) << '\t' << addr_ins_static_map[ins_addr]
                                  << std::endl;
      printed_instructions[ins_addr] = true;
    }
    
    // dynamic trace
//    dynamic_instruction_log_file << std::setfill(' ') << std::setw(30) << contained_library(ins_addr);
//    dynamic_instruction_log_file << std::setfill(' ') << std::setw(16);
    dynamic_instruction_log_file << reinterpret_cast<VOID*>(ins_addr) << '\t' << addr_ins_static_map[ins_addr]
                                 << std::endl;
  }

//  if (logged_instructions.size() >= 7000) {
//     stop_tracing(0, 0);
//     exit(0);
//     PIN_ExitApplication(0);
//  }
  
  return;
}

/* ------------------------------------------------------------------------- */
/* log dynamic and static trace with dereference of indirect jump            */
/* ------------------------------------------------------------------------- */
void log_static_and_dynamic_dereference(ADDRINT ins_addr, ADDRINT jmp_addr)
{
  if (first_message_received) {
    std::istringstream iss(addr_ins_static_map[ins_addr]);
    std::string operand; iss >> operand;

    // static trace
    if (printed_instructions.find(ins_addr) == printed_instructions.end()) {
//      static_instruction_log_file << std::setfill(' ') << std::setw(30) << contained_library(ins_addr);
//      static_instruction_log_file << std::setfill(' ') << std::setw(16);
      static_instruction_log_file << reinterpret_cast<VOID*>(ins_addr) << '\t';

      if (instruction_type[ins_addr] == INDIRECT_JUMP_INSTRUCTION ||
          instruction_type[ins_addr] == RETURN_INSTRUCTION ||
          instruction_type[ins_addr] == CALL_INSTRUCTION) {
        static_instruction_log_file << operand << " " << reinterpret_cast<VOID*>(jmp_addr);
      }
      else {
        static_instruction_log_file << addr_ins_static_map[ins_addr];
      }
      static_instruction_log_file << std::endl;

//      static_instruction_log_file << '\t' << reinterpret_cast<VOID*>(jmp_addr) << std::endl;
      printed_instructions[ins_addr] = true;
    }

    // dynamic trace
//    dynamic_instruction_log_file << std::setfill(' ') << std::setw(30) << contained_library(ins_addr);
//    dynamic_instruction_log_file << std::setfill(' ') << std::setw(16);
    dynamic_instruction_log_file << reinterpret_cast<VOID*>(ins_addr) << '\t';

    if (instruction_type[ins_addr] == INDIRECT_JUMP_INSTRUCTION ||
        instruction_type[ins_addr] == RETURN_INSTRUCTION ||
        instruction_type[ins_addr] == CALL_INSTRUCTION) {
      dynamic_instruction_log_file << operand << " " << reinterpret_cast<VOID*>(jmp_addr);
    }
    else {
      dynamic_instruction_log_file << addr_ins_static_map[ins_addr];
    }
    dynamic_instruction_log_file << std::endl;

//    dynamic_instruction_log_file << '\t' <<  reinterpret_cast<VOID*>(jmp_addr) << std::endl;
  }
  return;
}


/*===================================================================================================================*/
/*                                                 instrumentation routines                                          */
/*===================================================================================================================*/


/* ------------------------------------------------------------------------- */
/* before tracing                                                            */
/* ------------------------------------------------------------------------- */
VOID start_tracing(VOID *data)
{
  std::stringstream ss;
  ss << std::dec << PIN_GetPid();
  std::string pid = ss.str(); 
  
  log_file.open((pid + "network_syscall.log").c_str());
  log_file.setf(std::ios::left);

  dynamic_instruction_log_file.open((pid + "network_instruction_dynamic.log").c_str());
  dynamic_instruction_log_file.setf(std::ios::left);

  static_instruction_log_file.open((pid + "network_instruction_static.log").c_str());
  static_instruction_log_file.setf(std::ios::left);
  return;
}

/* ------------------------------------------------------------------------- */
/* after tracing                                                             */
/* ------------------------------------------------------------------------- */
void stop_tracing(INT32 code, VOID *data)
{
  log_file.close();
  dynamic_instruction_log_file.close();
  static_instruction_log_file.close();
  return;
}

/* ------------------------------------------------------------------------- */
/* called just before syscall                                                */
/* ------------------------------------------------------------------------- */
VOID syscall_entry(THREADID thread_index, CONTEXT *context, SYSCALL_STANDARD standard, VOID *data)
{
  syscall_id = PIN_GetSyscallNumber(context, standard);
  
  /* get basic arguments */
  for (unsigned int i = 0; i < 6; i++) {
    args[i] = PIN_GetSyscallArgument(context, standard, i);
  }
    
  return;
}

/* ------------------------------------------------------------------------- */
/* called just after syscall                                                 */
/* ------------------------------------------------------------------------- */
VOID syscall_exit(THREADID thread_index, CONTEXT *context, SYSCALL_STANDARD standard, VOID *data)
{
  switch (syscall_id) {
//     case sendto_id: log_sendto(); break;
    
    case recvfrom_id: 
      if (!first_message_received) {
        mod_recvfrom();
        log_recvfrom(); 
        first_message_received = true;
      }
      else {
//         stop_tracing(0, 0);
//         exit(0);
        PIN_ExitApplication(0);
      }
      break;
    
    default: break;
  }
  
  return;
}

/* ------------------------------------------------------------------------- */
/* called before an instruction is executed                                  */
/* ------------------------------------------------------------------------- */
VOID instruction_rw(INS ins, VOID *value)
{
  return;
}

/* ------------------------------------------------------------------------- */
/* called before (IPOINT_BEFORE) an instruction is executed                  */
/* ------------------------------------------------------------------------- */
VOID instruction(INS ins, VOID *value)
{
  ADDRINT instruction_addr = INS_Address(ins);

  // dont care for self-modifying code
  addr_ins_static_map[instruction_addr] = INS_Disassemble(ins);

  /*if (INS_IsIndirectBranchOrCall(ins)) {
    if (INS_IsCall(ins)) {
      instruction_type[instruction_addr] = CALL_INSTRUCTION;
    }

    if (INS_IsBranch(ins)) {
      instruction_type[instruction_addr] = INDIRECT_JUMP_INSTRUCTION;
    }

    if (INS_IsRet(ins)) {
      instruction_type[instruction_addr] = RETURN_INSTRUCTION;
    }

    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)log_static_and_dynamic_dereference, IARG_INST_PTR,
                   IARG_BRANCH_TARGET_ADDR, IARG_END);
  }
  else {
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)log_static_and_dynamic, IARG_INST_PTR, IARG_END);
  }*/
  INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)log_static_and_dynamic, IARG_INST_PTR, IARG_END);
  return;
}


/*===================================================================================================================*/
/*                                                       main function                                               */
/*===================================================================================================================*/
int main(int argc, char* argv[])
{
  PIN_Init(argc, argv);
  
  PIN_AddApplicationStartFunction(start_tracing, 0);
  PIN_AddSyscallEntryFunction(syscall_entry, 0);
  PIN_AddSyscallExitFunction(syscall_exit, 0);
//   INS_AddInstrumentFunction(instruction_rw, 0);
  INS_AddInstrumentFunction(instruction, 0);
  PIN_AddFiniFunction(stop_tracing, 0);
  
  PIN_StartProgram();
  return 0;
}
