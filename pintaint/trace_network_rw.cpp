#include "pin.H"
#include "instlib.H"
#include "portability.H"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <map>
#include <sstream>
#include <cstdlib>
#include <ctime>


// -----------------------------------------------------------------------------
//
//                           global variables
//
// -----------------------------------------------------------------------------
#define MODIFIED_BYTE_NUMBER 50

// some instruction types
#define CALL          0
#define INDIRECT_JUMP 1
#define RETURN        2

std::map<ADDRINT, unsigned char> ins_type;

// syscall number
#define SENDTO   44
#define RECVFROM 45

ADDRINT syscall_id;

// arguments of each invoked syscall
ADDRINT args[6];

// buffer storing received data
char        *buffer_data   = 0;
unsigned int buffer_length = 0;

ADDRINT buff_addr = 0;
UINT32  buff_size = 0;
std::map<ADDRINT, bool> buff_marked;

// log files
std::ofstream log_file;
std::ofstream dyn_trace_log_file;
std::ofstream static_trace_log_file;
std::ofstream mem_access_log_file;

bool is_read = false;
bool is_write = false;

bool is_first_msg = true;
uint32_t recved_msg_num = 0;
uint32_t logged_ins_num = 0;

std::map<ADDRINT, std::string> addr_ins_static_map;

/*
 * true:  the instruction has been logged,
 * false: otherwise
 */
std::map<ADDRINT, bool> printed_inss;


// ----------------------------------------------------------------------------
//                               input handler routines
// ----------------------------------------------------------------------------
KNOB<uint32_t> log_ins_max_num(KNOB_MODE_WRITEONCE,
                               "pintool",
                               "n", "20000",
                               "specify number of logged instruction");



// ----------------------------------------------------------------------------
//
//                            tool functions
//
// ----------------------------------------------------------------------------
/*
 * log the recvfrom syscall
 */
void log_recvfrom()
{
  recved_msg_num++;

  log_file << "address: " << reinterpret_cast<VOID*>(args[1])                   // write out
           << "\tsize: " << args[2] << std::endl;

  if (recved_msg_num == 1)                                                      // focus on the first message
  {
    buff_addr = args[1]; buff_size = args[2];

    for (uint32_t mem_idx = 0; mem_idx < buff_size; ++mem_idx)                  // mark the buffer as not accessed yet
    {
      buff_marked[buff_addr + mem_idx] = false;
    }
  }

  return;
}


/*
 * detect the library being loaded at this address
 * (modified from the Zynamics shellcode detection tool)
 */
std::string contained_library(ADDRINT address)
{
  std::string lib_name = "";
  for (IMG img = APP_ImgHead(); IMG_Valid(img); img = IMG_Next(img))
  {
    for (SEC sec = IMG_SecHead(img); SEC_Valid(sec); sec = SEC_Next(sec))
    {
      if (address >= SEC_Address(sec) && address < SEC_Address(sec) + SEC_Size(sec))
      {
        lib_name = IMG_Name(img);
        break;
      }
    }
    if (lib_name != "") break;
  }

  return lib_name;
}


// ----------------------------------------------------------------------------
//
//                               analysis routines
//
// ----------------------------------------------------------------------------
/*
 * log dynamic trace
 */
void log_dynamic_trace(ADDRINT ins_addr)
{
  // start logging whenever the first message is received
  if (recved_msg_num >= 1)
  {
    dyn_trace_log_file << reinterpret_cast<VOID*>(ins_addr) << '\t'
                       << addr_ins_static_map[ins_addr] << std::endl;
    logged_ins_num++;
  }

  if (logged_ins_num >= log_ins_max_num.Value())
    PIN_ExitApplication(0);

  return;
}


/*
 * log instructions and their memory read/write addresses
 */
void log_ins_mem(ADDRINT ea_mem_addr, UINT32 ea_mem_size)
{
  if (recved_msg_num >= 1)                                                      // start logging whenever the fist message is received
  {    
    for (UINT32 mem_idx = 0; mem_idx < ea_mem_size; ++mem_idx)                  // mark the access addresses
      buff_marked[ea_mem_addr + mem_idx] = true;
  }

  return;
}


// ----------------------------------------------------------------------------
//
//                       instrumentation routines
//
// ----------------------------------------------------------------------------
/*
 * before tracing
 */
VOID start_tracing(VOID *data)
{
  std::stringstream ss;
  ss << std::dec << PIN_GetPid();
  std::string pid = ss.str(); 
  
  log_file.open((pid + "msg_recved.log").c_str());

  dyn_trace_log_file.open((pid + "instruction_traced.log").c_str());

  mem_access_log_file.open((pid + "mem_access.log").c_str());
//  mem_access_log_file.setf(std::ofstream::left);

  return;
}


/*
 * after tracing
 */
void stop_tracing(INT32 code, VOID *data)
{
  ADDRINT lower_addr, upper_addr;
  bool is_masked = false;

  // print the accessed addresses
  for (UINT32 mem_idx = 0; mem_idx < buff_size; ++mem_idx)
  {
    ADDRINT access_addr = buff_addr + mem_idx;                                  // verify each byte in the buffer of received message
    if (buff_marked[access_addr])                                               // (I) if the byte is read
    {
      if (is_masked)                                                            // (1) then this is an old memory range
      {
        upper_addr++;                                                           // so just increase the upper address,
      }
      else                                                                      // (2) else this is a new memory range
      {
        is_masked = true;                                                       // so mark it as old
        lower_addr = upper_addr = access_addr;                                  // and set the lower and the upper addresses.
      }
    }
    else                                                                        // (II) if the byte is not read
    {
      if (is_masked)                                                            // and the mask flag is previously set, i.e. a range
      {                                                                         // then print out the range.
        mem_access_log_file << upper_addr - lower_addr + 1 << "\t["
                            << reinterpret_cast<void*>(lower_addr) << ", "
                            << reinterpret_cast<void*>(upper_addr) << "]"
                            << "\t[" << lower_addr - buff_addr
                            << ", " << upper_addr - buff_addr << "]"
                            << std::endl;
      }
      is_masked = false;                                                        // and always reset the mask flag if the byte is not read.
    }
  }

  log_file.close();
  dyn_trace_log_file.close();
  mem_access_log_file.close();
  return;
}


/*
 * called just before a syscall is invoked.
 */
VOID syscall_entry(THREADID thread_index, CONTEXT *context,
                   SYSCALL_STANDARD standard, VOID *data)
{
  syscall_id = PIN_GetSyscallNumber(context, standard);
  
  /* get basic arguments */
  for (unsigned int i = 0; i < 6; i++)                                          // get first 6 arguments of any system call
  {
    args[i] = PIN_GetSyscallArgument(context, standard, i);
  }
    
  return;
}


/*
 * called just after a syscall is invoked.
 */
VOID syscall_exit(THREADID thread_index, CONTEXT *context,
                  SYSCALL_STANDARD standard, VOID *data)
{
  switch (syscall_id)
  {
    case RECVFROM:
      log_recvfrom(); break;
    default: break;
  }
  
  return;
}


/*
 * will insert instrumental routines before (IPOINT_BEFORE) an instruction is
 * executed.
 */
VOID instruction_mem(INS ins, VOID *value)
{
  ADDRINT ins_addr = INS_Address(ins);                                          // log instruction
  addr_ins_static_map[ins_addr] = INS_Disassemble(ins);

  INS_InsertPredicatedCall(ins, IPOINT_BEFORE,                                  // insert the instruction intrumental routine
                           (AFUNPTR)log_dynamic_trace,
                           IARG_INST_PTR,
                           IARG_END);

  UINT32 mem_op_num = INS_MemoryOperandCount(ins);                              // get the number of memory operands

  for (UINT32 mem_op_id = 0; mem_op_id < mem_op_num; ++mem_op_id)
  {                                                                             // insert the memory accessed instrumental routine
    if (INS_MemoryOperandIsRead(ins, mem_op_id))                                // whenever the instruction read memory
      INS_InsertPredicatedCall(ins, IPOINT_BEFORE,
                               (AFUNPTR)log_ins_mem,
                               IARG_MEMORYOP_EA, mem_op_id,
                               IARG_MEMORYREAD_SIZE,
                               IARG_END);
  }

  return;
}


// -----------------------------------------------------------------------------
//                                 main function
// -----------------------------------------------------------------------------


int main(int argc, char* argv[])
{
  PIN_Init(argc, argv);
  
  PIN_AddApplicationStartFunction(start_tracing, 0);
  PIN_AddSyscallEntryFunction(syscall_entry, 0);
  PIN_AddSyscallExitFunction(syscall_exit, 0);
  INS_AddInstrumentFunction(instruction_mem, 0);
  PIN_AddFiniFunction(stop_tracing, 0);
  
  PIN_StartProgram();
  return 0;
}
