#include "partial_trace.h"

#include <algorithm>
#include <sstream>
#include <boost/functional/hash.hpp>

/*===========================================================================*/

partial_trace::partial_trace()
{
}

/*===========================================================================*/

partial_trace::partial_trace(const ADDRINT& ins_addr, const ADDRINT& br_addr,
                             const ADDRINT& next_addr, const bool& br_taken,
                             const addresses& trace)
{
  this->ins_addr = ins_addr;
  this->br_addr = br_addr;
  this->next_addr = next_addr;
  this->br_taken = br_taken;
  this->trace = trace;
}

/*===========================================================================*/

std::string partial_trace::to_string()
{
  std::stringstream ss;
  ss << reinterpret_cast<void*>(ins_addr) << " "
     << reinterpret_cast<void*>(br_addr) << " "
     << reinterpret_cast<void*>(next_addr) << " "
     << br_taken;

  for (addresses_iter addr_iter = trace.begin(); addr_iter != trace.end();
       ++addr_iter)
  {
    ss << " " << reinterpret_cast<void*>(*addr_iter);
  }
  return ss.str();
}

/*===========================================================================*/

std::string partial_trace::to_string(instruction_map& ins_opcodes)
{
  std::stringstream ss;
  ss << reinterpret_cast<void*>(ins_addr) << " "
     << reinterpret_cast<void*>(br_addr) << " "
     << reinterpret_cast<void*>(next_addr) << " "
     << br_taken;

  ss << "\n";
  for (addresses_iter addr_iter = trace.begin(); addr_iter != trace.end();
       ++addr_iter)
  {
    ss << ins_opcodes[*addr_iter].disass << "\n";
  }

  return ss.str();
}
