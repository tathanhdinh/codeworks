#ifndef PARTIAL_TRACE_H
#define PARTIAL_TRACE_H

#include "instruction.h"
#include <pin.H>
#include <string>
#include <boost/cstdint.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <boost/shared_ptr.hpp>

class partial_trace;

typedef std::vector<partial_trace>          partial_traces;
typedef std::vector<checkpoint>             checkpoints;
typedef std::pair<partial_trace, UINT8>     reachable_trace;
typedef std::vector<reachable_trace>        reachable_traces;

typedef partial_traces::iterator            partial_traces_iter;
typedef reachable_traces::iterator          reachable_traces_iter;

class partial_trace
{
public:
  partial_trace();
  partial_trace(const ADDRINT& ins_addr, const ADDRINT& br_addr,
                const ADDRINT& next_addr, const bool& br_taken,
                const addresses& trace);

  std::string to_string();
  std::string to_string(instruction_map& ins_opcodes);

  ADDRINT              ins_addr;
  ADDRINT              br_addr;
  ADDRINT              next_addr;
  bool                 br_taken;
  addresses            trace;
};

// inline bool operator==(const partial_trace& trace_a, 
//                        const partial_trace& trace_b);
// inline bool operator< (const partial_trace& trace_a, const partial_trace& trace_b);
// 
// inline partial_traces_iter pos_in(partial_traces& traces, 
//                                   const ADDRINT& ins_addr, 
//                                   const ADDRINT& br_addr, const bool& br_taken, 
//                                   const addresses& trace);

/*===========================================================================*/

inline bool operator==(const partial_trace& trace_a, const partial_trace& trace_b)
{
  return (trace_a.ins_addr == trace_b.ins_addr) &&
      (trace_a.br_taken == trace_b.br_taken) &&
      (trace_a.trace.size() == trace_b.trace.size()); 
//       std::equal(trace_a.trace.begin(), trace_a.trace.end(),
//                  trace_b.trace.begin());
}

/*===========================================================================*/

inline bool operator< (const partial_trace& trace_a, const partial_trace& trace_b)
{
  return (trace_a.trace.size() < trace_b.trace.size());
}

/*===========================================================================*/

inline partial_traces_iter pos_in(partial_traces& traces, 
                           const ADDRINT& ins_addr, 
                           const ADDRINT& br_addr, const bool& br_taken, 
                           const addresses& trace)
{
  partial_traces_iter trace_iter;
  for (trace_iter = traces.begin(); trace_iter != traces.end(); ++trace_iter)
  {
    if ((trace_iter->ins_addr == ins_addr) && 
        (trace_iter->br_addr == br_addr) && 
        (trace_iter->br_taken == br_taken) && 
        (trace_iter->trace.size() == trace.size()))
    {
      break;
    }
  }
  return trace_iter;
}

#endif // PARTIAL_TRACE_H
