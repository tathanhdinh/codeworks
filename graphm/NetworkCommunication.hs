module NetworkCommunication where

import qualified Network
import qualified System.IO

main :: IO ()
main = do 
  inputRepos <- System.IO.getLine
  getCommand inputRepos
  
getCommand :: String -> IO ()
getCommand repos = do 
  handle <- Network.connectTo repos (Network.PortNumber 80)
  System.IO.hClose handle

